import {Component} from 'angular2/core';

@Component({
    selector: 'app',
    template: '<h1>Angular 2 client</h1>'
})
export class AppComponent { }
